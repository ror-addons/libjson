<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="LibJson" version="2.2" date="01/01/2020">
		<Author name="Johndon" email="addon@rorah.com" />
		<Description text="For addon developers to encode LUA data structures in json format or decode strings into LUA data structures" />
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" />
		<Dependencies>
			<Dependency name="LibStub" />
        </Dependencies>

		<Files>
			<File name="Json.lua" />
		</Files>
	</UiMod>
</ModuleFile>